#ifndef _MATCH_H
#define _MATCH_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Player
{
	int Lost;
	int Won;
	int Points;
} Player;

/**
 * @brief Initializes a player object
 * @param player The player object
 */
void Player_init(Player* player);

/**
 * @brief Updates the player data
 * @param player The player to edit
 * @param points The points to add
 */
void Player_win(Player* player, int points);

/**
 * @brief Updates the player data
 * @param player The player to edit
 */
void Player_loose(Player* player);

typedef struct Match
{
	Player* Players;
	int PlayerCount;
	int ActivePlayer;
	int MaxPoints;
} Match;

/**
 * @brief Initializes a match object
 * @param match The match object
 * @param playerCount The number of players to use
 * @param maxPoints The number of max points
 */
void Match_init(Match* match, int playerCount, int maxPoints);

/**
 * @brief Frees all ressources used by the match object
 * @param match The match object
 */
void Match_destroy(Match* match);

/**
 * @brief Forwards to the next player
 * @param match The match object
 * @return 1 if there will be a next round otherwise 0
 */
int Match_nextPlayer(Match* match);

/**
 * @brief Makes the active player win
 * @param match The match object
 * @param points The points to add
 */
void Match_playerWin(Match* match, int points);

/**
 * @brief Makes the active playe loose
 * @param match The match object
 */
void Match_playerLost(Match* match);

/**
 * @brief Returns the player index with the most points
 * @param match The match object
 * @return Index of the player
 */
int Match_playerIndexWithHighestPoints(Match match);

#ifdef __cplusplus
}
#endif

#endif // _MATCH_H