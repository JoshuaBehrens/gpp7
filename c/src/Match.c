#include "Match.h"

void Player_init(Player* player)
{
	player->Lost = 0;
	player->Points = 0;
	player->Won = 0;
}

void Player_win(Player* player, int points)
{
	++player->Won;
	player->Points += points;
}

void Player_loose(Player* player)
{
	++player->Lost;
}

void Match_init(Match* match, int playerCount, int maxPoints)
{
	match->ActivePlayer = 0;
	match->MaxPoints = maxPoints;
	match->PlayerCount = playerCount;
	match->Players = (Player*)malloc(sizeof(Player) * playerCount);
}

void Match_destroy(Match* match)
{
	free(match->Players);
	match->PlayerCount = 0;
	match->ActivePlayer = 0;
}

int Match_nextPlayer(Match* match)
{
	if (++match->ActivePlayer == match->PlayerCount)
	{
		int maxPlayer = Match_playerIndexWithHighestPoints(*match);
		if (match->Players[maxPlayer].Points >= match->MaxPoints)
			return 0;
		
		match->ActivePlayer = 0;
	}
	
	return 1;
}

void Match_playerWin(Match* match, int points)
{
	Player_win(&match->Players[match->ActivePlayer], points);
}

void Match_playerLost(Match* match)
{
	Player_loose(&match->Players[match->ActivePlayer]);
}

int Match_playerIndexWithHighestPoints(Match match)
{
	int result = 0;

	for (int c = 1; c < match.PlayerCount; ++c)
		if (match.Players[c].Points > match.Players[result].Points)
			result = c;
	
	return result;
}
