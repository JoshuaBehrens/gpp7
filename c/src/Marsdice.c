#include <stdlib.h>

#include "Marsdice.h"

int randomInt(int min, int max)
{
	return rand() % (max - min) + min;
}

void Dice_init(Dice* dice)
{
	dice->Face = Undefined;
	dice->IsTaken = 0;
}

DiceFace Dice_roll(Dice* dice)
{
	if (dice->IsTaken == 0)
	{
		int face = randomInt(0, 6);
		if (face == 0)
			return dice->Face = Chicken;
		if (face == 1)
			return dice->Face = Cow;
		if (face == 2)
			return dice->Face = Human;
		if (face == 3)
			return dice->Face = Troop;
		return dice->Face = UFO;
	}
	
	return dice->Face;
}

void RoundState_init(RoundState* state)
{
	state->ChosenUFOs = 0;
	state->ChosenCows = 0;
	state->ChosenHumans = 0;
	state->ChosenChickens = 0;
	state->EncounteredTroops = 0;
	state->DicesLeft = 13;

	for (int c = 0; c < 13; ++c)
		Dice_init(&state->Dices[c]);
}

void RoundState_nextStep(RoundState* state)
{
	for (int c = 0; c < 13; ++c)
		if (RoundState_isDiceRollable(*state, c))
		{
			if (Dice_roll(&state->Dices[c]) == Troop)
			{
				++state->EncounteredTroops;
				--state->DicesLeft;
				state->Dices[c].IsTaken = 1;
			}
		}
}

RoundStateOption RoundState_availableOptions(RoundState state)
{
	RoundStateOption result = ChooseNone;
	
	if (state.DicesLeft == 0)
		return result;

	if (state.ChosenChickens == 0 && RoundState_countDices(state, Chicken, 1))
		result |= ChooseChickens;
	if (state.ChosenCows == 0 && RoundState_countDices(state, Cow, 1))
		result |= ChooseCows;
	if (state.ChosenHumans == 0 && RoundState_countDices(state, Human, 1))
		result |= ChooseHumans;
	if (RoundState_countDices(state, UFO, 1))
		result |= ChooseUFOs;

	return result;
}

int RoundState_isDiceRollable(RoundState state, int index)
{
	return (-1 < index && index < 13 && state.Dices[index].IsTaken == 1) ? 0 : 1;
}

void RoundState_doOption(RoundState* state, RoundStateOption option)
{
	RoundStateOption possible = RoundState_availableOptions(*state);
	if (possible & option)
	{
		if (option == ChooseChickens)
		{
			for (int c = 0; c < 13; ++c)
				if (RoundState_isDiceRollable(*state, c) && state->Dices[c].Face == Chicken)
				{
					++state->ChosenChickens;
					--state->DicesLeft;
					state->Dices[c].IsTaken = 1;
				}
		}
		else if (option == ChooseCows)
		{
			for (int c = 0; c < 13; ++c)
				if (RoundState_isDiceRollable(*state, c) && state->Dices[c].Face == Cow)
				{
					++state->ChosenCows;
					--state->DicesLeft;
					state->Dices[c].IsTaken = 1;
				}
		}
		else if (option == ChooseHumans)
		{
			for (int c = 0; c < 13; ++c)
				if (RoundState_isDiceRollable(*state, c) && state->Dices[c].Face == Human)
				{
					++state->ChosenHumans;
					--state->DicesLeft;
					state->Dices[c].IsTaken = 1;
				}
		}
		else if (option == ChooseUFOs)
		{
			for (int c = 0; c < 13; ++c)
				if (RoundState_isDiceRollable(*state, c) && state->Dices[c].Face == UFO)
				{
					++state->ChosenUFOs;
					--state->DicesLeft;
					state->Dices[c].IsTaken = 1;
				}
		}
	}
}

int RoundState_countDices(RoundState state, DiceFace face, int rollable)
{
	int result = 0;
	
	for (int c = 0; c < 13; ++c)
		if (face == state.Dices[c].Face)
			if (rollable == 0 || RoundState_isDiceRollable(state, c))
				++result;
	
	return result;
}

int RoundState_won(RoundState state)
{
	return state.ChosenUFOs >= state.EncounteredTroops;
}
