#ifndef _MARSDICE_H
#define _MARSDICE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum RoundStateOption
{
	ChooseNone = 0,
	ChooseUFOs = 1,
	ChooseCows = 2,
	ChooseHumans = 4,
	ChooseChickens = 8
} RoundStateOption;

typedef enum DiceFace
{
	Undefined,
	Cow,
	Human,
	Chicken,
	UFO,
	Troop
} DiceFace;

typedef struct Dice
{
	DiceFace Face;
	int IsTaken;
} Dice;

/**
 * @brief Initializes a dice
 * @param dice The dice to initilize
 */
void Dice_init(Dice* dice);

/**
 * @brief Rolls the dice
 * @param dice The dice to roll
 * @return The rolled face
 */
DiceFace Dice_roll(Dice* dice);

typedef struct RoundState
{
    int ChosenUFOs;
    int ChosenCows;
    int ChosenHumans;
    int ChosenChickens;
    int EncounteredTroops;
    int DicesLeft;
	Dice Dices[13];
} RoundState;

/**
 * @brief Initializes a state
 * @param state The state to initialize
 */
void RoundState_init(RoundState* state);

/**
 * @brief Takes the next step for this state
 * @param state The state to forward
 */
void RoundState_nextStep(RoundState* state);

/**
 * @brief Returns a flag enum saying which options are available
 * @param state The state to check
 * @return Flag enum of available options
 */
RoundStateOption RoundState_availableOptions(RoundState state);

/**
 * @brief Checks whether the n-th dice can be rolled
 * @param state The state to check
 * @param index The dice index
 * @return 1 if the dice is rollable otherwise 0
 */
int RoundState_isDiceRollable(RoundState state, int index);

/**
 * @brief Performs the given option on the state
 * @param state The state to change
 * @param option The option to do
 */
void RoundState_doOption(RoundState* state, RoundStateOption option);

/**
 * @brief Counts all dices with the given face
 * @param state The state to check
 * @param face The face to check
 * @param rollable if 1 the dices that are counted have to rollable
 * @return The number of dices to fit the parameters
 */
int RoundState_countDices(RoundState state, DiceFace face, int rollable);

/**
 * @brief Checks whether this state won
 * @param state The state to check
 * @return 1 if won otherwise 0
 */
int RoundState_won(RoundState state);

#ifdef __cplusplus
}
#endif

#endif // _MARSDICE_H
