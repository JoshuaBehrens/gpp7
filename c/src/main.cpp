#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Marsdice.h"
#include "Match.h"

std::ostream& operator<<(std::ostream& out, RoundState& state)
{
    out << "Chicken:\t" << state.ChosenChickens << std::endl;
    out << "Cows:\t\t" << state.ChosenCows << std::endl;
    out << "Humans:\t\t" << state.ChosenHumans << std::endl;
    out << "UFOs:\t\t" << state.ChosenUFOs << std::endl;
    out << "Troops:\t\t" << state.EncounteredTroops << std::endl;
    //	out << "Dices[" << state.DicesLeft << "]\t";
    //	for (int c = 0; c < 13; c++)
    //		if (RoundState_isDiceRollable(state, c))
    //			out << " " << state.Dices[c];
    return out;
}

RoundStateOption menu(RoundState& state)
{
    while(true)
	{
		std::cout << "Choices:" << std::endl;
		RoundStateOption opt = RoundState_availableOptions(state);
		if (opt == ChooseNone)
			return opt;
		if (opt & ChooseChickens)
			std::cout << "\tChickens\t[k] " << RoundState_countDices(state, Chicken, 1) << std::endl;
		if (opt & ChooseUFOs)
			std::cout << "\tUFOs\t\t[u] " << RoundState_countDices(state, UFO, 1) << std::endl;
		if (opt & ChooseHumans)
			std::cout << "\tHumans\t\t[h] " << RoundState_countDices(state, Human, 1) << std::endl;
		if (opt & ChooseCows)
			std::cout << "\tCows\t\t[c] " << RoundState_countDices(state, Cow, 1) << std::endl;
		std::cout << "\tQuit\t\t[q] " << (RoundState_won(state) ? "Winning": "Losing")<< std::endl;
	
		char option = 0;
		std::cin >> option;
	
		switch(option)
		{
		case 'k':
			if(opt & ChooseChickens)
				return ChooseChickens;
			break;
		case 'u':
			if(opt & ChooseUFOs)
				return ChooseUFOs;
			break;
		case 'h':
			if(opt & ChooseHumans)
				return ChooseHumans;
			break;
		case 'c':
			if(opt & ChooseCows)
				return ChooseCows;
			break;
		case 'q':
			return ChooseNone;
		}
    }
}

int main(int argc, char** argv)
{
    srand(time(0));

	Match match;
	Match_init(&match, 2, 10);
	
	do
	{
		std::cout << "Next player is " << (match.ActivePlayer + 1) << std::endl;
		
		RoundState state;
		RoundState_init(&state);

		RoundStateOption options;
		do
		{
			RoundState_nextStep(&state);
			std::cout << state << std::endl;
			RoundState_doOption(&state, menu(state));
			options = RoundState_availableOptions(state);
		} while(options != ChooseNone);

		if (RoundState_won(state))
		{
			std::cout << "Victorious";
			if (state.ChosenChickens * state.ChosenCows * state.ChosenHumans > 0)
				Match_playerWin(&match, state.ChosenChickens + state.ChosenCows + state.ChosenHumans + 3);
			else
				Match_playerWin(&match, state.ChosenChickens + state.ChosenCows + state.ChosenHumans);
		}
		else
		{
			std::cout << "Lost";
			Match_playerLost(&match);
		}
		std::cout << std::endl;
	} while (Match_nextPlayer(&match));

	std::cout << "Player " << (Match_playerIndexWithHighestPoints(match) + 1) << " won" << std::endl;

	Match_destroy(&match);

    return 0;
}
