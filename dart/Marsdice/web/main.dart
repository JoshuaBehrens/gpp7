import 'dart:html';

import 'package:Marsdice/game.dart';
import 'package:route_hierarchical/client.dart';

void main() {
  initGame();

  // Webapps need routing to listen for changes to the URL.
  var router = new Router();
  router.root
    ..addRoute(name: 'home', defaultRoute: true, path: '/', enter: showGame);
  router.listen();
}

void showGame(RouteEvent e) {
  // Extremely simple and non-scalable way to show different views.
  querySelector('.route_page').style.display = 'none';
  querySelector('#game_page').style.display = '';
}
