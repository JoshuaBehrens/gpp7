library logic;

import 'dart:math';

enum RoundStateOption {
  ChooseNone,
  ChooseUFOs,
  ChooseCows,
  ChooseHumans,
  ChooseChickens
}

enum DiceFace { Undefined, Cow, Human, Chicken, UFO, Troop }

class Dice {
  Random _randomGenerator;
  DiceFace _face;
  bool _isTaken;

  Dice() {
    this._face = DiceFace.Undefined;
    this._isTaken = false;
    this._randomGenerator = new Random();
  }

  void Take() {
    if (!this._isTaken) {
      this._isTaken = true;
    }
  }

  bool IsTaken() => this._isTaken;

  DiceFace Roll() {
    if (!this._isTaken) {
      switch (this._randomGenerator.nextInt(6)) {
        case 0:
          return this._face = DiceFace.Chicken;
        case 1:
          return this._face = DiceFace.Cow;
        case 2:
          return this._face = DiceFace.Human;
        case 3:
          return this._face = DiceFace.Troop;
        case 4:
        case 5:
          return this._face = DiceFace.UFO;
        default:
          return this._face = DiceFace.Undefined;
      }
    }

    return this._face;
  }

  DiceFace Face() => this._face;
}

class RoundState {
  List<Dice> _dices;

  RoundState() {
    this._dices =
        new List<Dice>.generate(13, (int e) => new Dice(), growable: false);
  }

  Dice At(int n) {
    if (0 <= n && n <= 13) {
      return this._dices[n];
    }

    return null;
  }

  int AmountChoosen(DiceFace face) {
    int result = 0;
    this._dices.forEach((Dice d) {
      if (d.IsTaken() && d.Face() == face) result++;
    });
    return result;
  }

  int AmountAvailable(DiceFace face) {
    int result = 0;
    this._dices.forEach((Dice d) {
      if (!d.IsTaken() && d.Face() == face) result++;
    });
    return result;
  }

  int DicesLeft() {
    int result = 0;
    this._dices.forEach((Dice d) {
      if (!d.IsTaken()) result++;
    });
    return result;
  }

  void NextStep() {
    this._dices.forEach((Dice d) {
      if (d.Roll() == DiceFace.Troop) {
        d.Take();
      }
    });
  }

  List<RoundStateOption> AvailableOptions() {
    List<RoundStateOption> result = new List<RoundStateOption>();

    if (this.DicesLeft() == 0) return result;

    if (this.AmountChoosen(DiceFace.Chicken) == 0 &&
        this.AmountAvailable(DiceFace.Chicken) > 0) result
            .add(RoundStateOption.ChooseChickens);
    if (this.AmountChoosen(DiceFace.Cow) == 0 &&
        this.AmountAvailable(DiceFace.Cow) > 0) result
            .add(RoundStateOption.ChooseCows);
    if (this.AmountChoosen(DiceFace.Human) == 0 &&
        this.AmountAvailable(DiceFace.Human) > 0) result
            .add(RoundStateOption.ChooseHumans);
    if (this.AmountAvailable(DiceFace.UFO) > 0) result
        .add(RoundStateOption.ChooseUFOs);

    return result;
  }

  void DoOption(RoundStateOption option) {
    if (this.AvailableOptions().any((test) => test == option)) {
      switch (option) {
        case RoundStateOption.ChooseChickens:
          this._dices.forEach((Dice d) {
            if (!d.IsTaken() && d.Face() == DiceFace.Chicken) d.Take();
          });
          break;
        case RoundStateOption.ChooseCows:
          this._dices.forEach((Dice d) {
            if (!d.IsTaken() && d.Face() == DiceFace.Cow) d.Take();
          });
          break;
        case RoundStateOption.ChooseHumans:
          this._dices.forEach((Dice d) {
            if (!d.IsTaken() && d.Face() == DiceFace.Human) d.Take();
          });
          break;
        case RoundStateOption.ChooseUFOs:
          this._dices.forEach((Dice d) {
            if (!d.IsTaken() && d.Face() == DiceFace.UFO) d.Take();
          });
          break;
        case RoundStateOption.ChooseNone:
          break;
      }
    }
  }

  bool Won() =>
      this.AmountChoosen(DiceFace.UFO) > this.AmountChoosen(DiceFace.Troop);
}

class Player
{
  int _lost;
  int _won;
  int _points;
  
  int GamesLost() => this._lost;
  
  int GamesWon() => this._won;
  
  int Points() => this._points;
  
  Player() {
    this._lost = 0;
    this._won = 0;
    this._points = 0;
  }
  
  void Win(int points) {
    this._won++;
    this._points += points;
  }
  
  void Loose() {
    this._lost++;
  }
}

class GameMatch {
  List<Player> _players;
  int _activePlayer;
  int _maxPoints;
  
  GameMatch(int playerCount, this._maxPoints) {
    this._players = new List<Player>.generate(playerCount, (int index) => new Player(), growable: false);
    this._activePlayer = 0;
  }
  
  bool Next() {
    if (++this._activePlayer == this._players.length) {
      if (this.WinningPlayer().Points() >= this._maxPoints)
        return false;
      this._activePlayer = 0;
    }
    
    return true;
  }

  void PlayerWin(int points) {
    this._players[this._activePlayer].Win(points);
  }

  void PlayerLost() {
    this._players[this._activePlayer].Loose();    
  }

  int WinningPlayerIndex() {
    int result = 0;
    
    for (int i = 1; i < this._players.length; ++i)
      if (this._players[result].Points() < this._players[i].Points())
        result = i;
    
    return result;
  }

  Player WinningPlayer() {
    return this._players[this.WinningPlayerIndex()];
  }
} 

