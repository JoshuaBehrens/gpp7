library game;

import 'dart:html';
import 'package:Marsdice/logic.dart';

// Example of hooking into the DOM and responding to changes from input fields.
initGame() {
  GameMatch match = new GameMatch(2, 20);
  RoundState state = null;

  var troups = querySelector('#amount_troups');
  var cows = querySelector('#amount_cows');
  var chicken = querySelector('#amount_chicken');
  var humans = querySelector('#amount_humans');
  var ufos = querySelector('#amount_ufos');

  var buttonCows = querySelector('#button_cows');
  var buttonChicken = querySelector('#button_chicken');
  var buttonHumans = querySelector('#button_humans');
  var buttonUfos = querySelector('#button_ufos');

  var input = querySelector('#game_next');

  buttonCows.onClick.listen((_) {
    if (!buttonCows.classes.contains("button_disabled")) {
      state.DoOption(RoundStateOption.ChooseCows);
      buttonCows.text = state.AmountChoosen(DiceFace.Cow).toString();
      input.click();
    }
  });

  buttonChicken.onClick.listen((_) {
    if (!buttonChicken.classes.contains("button_disabled")) {
      state.DoOption(RoundStateOption.ChooseChickens);
      buttonChicken.text = state.AmountChoosen(DiceFace.Chicken).toString();
      input.click();
    }
  });

  buttonHumans.onClick.listen((_) {
    if (!buttonHumans.classes.contains("button_disabled")) {
      state.DoOption(RoundStateOption.ChooseHumans);
      buttonHumans.text = state.AmountChoosen(DiceFace.Human).toString();
      input.click();
    }
  });

  buttonUfos.onClick.listen((_) {
    if (!buttonUfos.classes.contains("button_disabled")) {
      state.DoOption(RoundStateOption.ChooseUFOs);
      buttonUfos.text = "Take (" + state.AmountChoosen(DiceFace.UFO).toString() + "already taken)";
      input.click();
    }
  });

  input.onClick.listen((_) {
    if (state == null) {
      state = new RoundState();
      state.NextStep();
      input.text = "Next";
      buttonChicken.text = "Take";
      buttonCows.text = "Take";
      buttonHumans.text = "Take";
      buttonUfos.text = "Take";
    } else if (state.AvailableOptions().length == 0) {
      if (state.Won()) {
        window.alert("Round won.");
        int chicks = state.AmountChoosen(DiceFace.Chicken);
        int cows = state.AmountChoosen(DiceFace.Cow);
        int humans = state.AmountChoosen(DiceFace.Human);
        match.PlayerWin(chicks + cows + humans + (chicks * cows * humans > 0 ? 3 : 0));
      } else {
        window.alert("Round lost.");
        match.PlayerLost();
      }
      if (match.Next()) {
        state = null;
        input.text = "Next player";
        buttonChicken.text = "-";
        if (!buttonChicken.classes.contains("button_disabled"))
          buttonChicken.classes.add("button_disabled");
        buttonCows.text = "-";
        if (!buttonCows.classes.contains("button_disabled"))
          buttonCows.classes.add("button_disabled");
        buttonHumans.text = "-";
        if (!buttonHumans.classes.contains("button_disabled"))
          buttonHumans.classes.add("button_disabled");
        buttonUfos.text = "-";
        if (!buttonUfos.classes.contains("button_disabled"))
          buttonUfos.classes.add("button_disabled");
        return;
      } else {
        window.alert("With " +
            match.WinningPlayer().Points().toString() +
            " points won the " +
            (match.WinningPlayerIndex() + 1).toString() +
            ". player.");
      }
    } else {
      state.NextStep();
    }

    troups.text = state.AmountChoosen(DiceFace.Troop).toString();
    cows.text = state.AmountAvailable(DiceFace.Cow).toString();
    humans.text = state.AmountAvailable(DiceFace.Human).toString();
    chicken.text = state.AmountAvailable(DiceFace.Chicken).toString();
    ufos.text = state.AmountAvailable(DiceFace.UFO).toString();

    var opts = state.AvailableOptions();

    if (opts.contains(RoundStateOption.ChooseChickens) ==
        buttonChicken.classes.contains("button_disabled")) buttonChicken.classes.toggle("button_disabled");

    if (opts.contains(RoundStateOption.ChooseCows) ==
        buttonCows.classes.contains("button_disabled")) buttonCows.classes.toggle("button_disabled");

    if (opts.contains(RoundStateOption.ChooseHumans) ==
        buttonHumans.classes.contains("button_disabled")) buttonHumans.classes.toggle("button_disabled");

    if (opts.contains(RoundStateOption.ChooseUFOs) ==
        buttonUfos.classes.contains("button_disabled")) buttonUfos.classes.toggle("button_disabled");
  });
}
